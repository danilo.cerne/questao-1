<?php  
function gera_numero($aQuantidade, $aMinimo, $aMaximo) {
   if( $aQuantidade > ($aMaximo - $aMinimo) ) {
       return false;
   }

   $numeros = range($aMinimo, $aMaximo);
   shuffle($numeros);
   array_splice($numeros, 0, $aQuantidade);
   sort($numeros);
   return $numeros;
}

//Execução
echo "<b>Saída em Array:</b> <br>";
var_dump(gera_numero(8, 2, 24));
echo "<br> <b>Saída em JSON:</b> <br>";
var_dump(json_encode(gera_numero(8, 2, 24)));

?>
